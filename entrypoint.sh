#!/bin/sh

echo "Waiting for postgres..."

while ! nc -z 0 5432; do
  sleep 0.1
  echo 'testing db'
done

sleep 3

echo "PostgreSQL started"

npm run start2
