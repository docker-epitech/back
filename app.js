const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');

const app = express();

const routes = require('./app/routes');

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const db = require('./app/models');

db.sequelize.sync().catch((err) => {
  console.error('An error occured during the connection to the database');
  console.error(err);
});

app.use('/', routes);

app.get('/', (req, res) => {
  console.log('Hello World requested');
  res.send('Hello World!');
});

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
  console.log(`Example app listening at http://localhost:${PORT}`);
});

module.exports = app;
