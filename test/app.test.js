const chai = require('chai');
const sinonChai = require('sinon-chai');
const supertest = require('supertest');
const app = require('../app');

const should = chai.should();

const server = supertest.agent(app);

chai.use(sinonChai);

describe('Index page test', () => {
  it('gets base url', (done) => {
    server
      .get('/')
      .expect(200)
      .end((err, res) => {
        res.status.should.be.equal(200);
        done();
      });
  });
});
