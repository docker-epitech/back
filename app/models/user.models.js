/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */
const { sequelize, Sequelize } = require('.');

const userModel = (sequelize, Sequelize) => {
  const User = sequelize.define('user', {
    name: {
      type: Sequelize.STRING,
    },
  });
  return User;
};

module.exports = userModel;
