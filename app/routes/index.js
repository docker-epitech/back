// Bring in the express server
const express = require('express');

// Bring in the Express Router
const router = express.Router();

// Import the Controller
const controller = require('../controllers');

// Create a new Note
router.post('/create', controller.create);

// Get all Notes
router.get('/getall', controller.findAll);

module.exports = router;
